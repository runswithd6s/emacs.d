;; https://github.com/stanaka/dash-at-point#readme
(global-set-key "\C-cd" 'dash-at-point)
(global-set-key "\C-ce" 'dash-at-point-with-docset)
