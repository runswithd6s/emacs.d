;; Start-up in workgroups mode
(workgroups-mode 1)

;; Load my list of workgroups
(wg-load "~/.emacs.d/private/workgroups.list")
